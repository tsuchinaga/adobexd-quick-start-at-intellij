const {Rectangle, Color} = require("scenegraph");

/**
 * @param {Selection} selection
 */
function rectangleHandlerFunction(selection) {
    const newElement = new Rectangle();
    newElement.width = 100;
    newElement.height = 50;
    newElement.fill = new Color("Purple");

    let artboard = selection.insertionParent;
    if ("addChild" in artboard) {
        artboard.addChild(newElement);
        newElement.moveInParentCoordinates(100, 100);
    }
}

module.exports = {
    commands: {
        createRectangle: rectangleHandlerFunction
    }
};
